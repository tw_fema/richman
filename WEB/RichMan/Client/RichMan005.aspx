﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RichMan/Client/WebLayout.Master" AutoEventWireup="true" CodeBehind="RichMan005.aspx.cs" Inherits="WEB.RichMan.Client.RichMan005" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            
        </Scripts>
    </asp:ScriptManager>
    
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#menu a:eq(0)").addClass("on");
        })
    </script>
   

<div class="content">
<h1>「土石流防災大富翁」申請須知:</h1>
    <img src="images/大型土石流防災大富翁宣導版.jpg" align="right" alt="大型土石流防災大富翁宣導版" />
<h2>一、大型土石流防災大富翁(活動宣導用) </h2>
<p>本局為推廣水土保持及土石流防災教育，製作土石流防災大富翁大型遊戲底圖，尺寸為4公尺×2.5公尺，並免費提供電子圖檔，如欲自行印製用於舉辦相關教育宣導活動者，請電洽(049-23475142 郭力行工程員) </p>
<h2>二、土石流防災大富翁盒裝版 </h2>
<ol >
  <li>索取條件： 為推廣水土保持及土石流防災教育，本局提供土石流防災大富翁盒裝版免費索取，可作為自然科教學輔助教材，歡迎教育推廣或教學單位踴躍索取；因數量有限，每個單位一次最多申請數量限30份，贈完為止！ </li>
  <li>申請方式： 請直接於本網站「線上申請」登記申請單位、需求份數等相關資料，本局將統一辦理郵寄程序。 </li>
  <!--<li>若運用土石流大富翁作為環境教育輔助教材，敬請將推廣經驗及教學心得連同照片上傳至本網站「意見分享」，本局將致贈精美小禮物一份；針對本教材若發現任何問題或有任何建議，亦請不吝指教，謝謝！ </li>-->
  <li>運用土石流防災大富翁遊戲作為防災教育推廣輔助教材，針對本教材若發現任何問題或有任何建議，亦請不吝指教，謝謝！ </li><img class="none" src="images/土石流防災大富翁盒裝印刷版.jpg" alt="土石流防災大富翁盒裝印刷版" />
</ol>
    
</div><!-- wrap -->   
<div class="wrap">
    
</div><!-- wrap -->    
</asp:Content>
