﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB.RichMan.Client
{
    public partial class RichMan003 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                setddlYear();

                //預設為最新一年資料
                int nowYear = DateTime.Now.Year;
                ddlYear.SelectedValue = nowYear.ToString();


                Query();

            }

        }

        void setddlYear()
        {
            
            if (ddlYear.Items.Count > 0)
                ddlYear.Items.Clear();
            DataTable dt = Entity.RichMan.RichMan.GetClientBearYearList();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlYear.Items.Add(new ListItem((Convert.ToInt32(dr["createDate"]) - 1911).ToString(), dr["createDate"].ToString()));
                }
            }
            ddlYear.Items.Insert(0, new ListItem("全部", "all"));
             

        }

        void Query()
        {
            
            DataTable dt = Entity.RichMan.RichMan.GetClientBearList(ddlYear.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                gvBearList.DataSource = dt;
                gvBearList.DataBind();
            }
            UpdatePanel1.Update();
             


        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void gvBearList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Web.UI.WebControls.Label lblProcessStatusTxt = e.Row.FindControl("lblProcessStatusTxt") as Label;
                System.Web.UI.WebControls.HiddenField hiddenSendDate = e.Row.FindControl("hiddenSendDate") as HiddenField;
                if (lblProcessStatusTxt.Text == "已送出")
                {
                    //lblProcessStatusTxt.Text += "<br/>寄送時間:" + (Convert.ToDateTime(hiddenSendDate.Value).Year - 1911).ToString() + "/" + Convert.ToDateTime(hiddenSendDate.Value).ToString("MM/dd HH:mm");
                }
                else if (lblProcessStatusTxt.Text == "重複申請")
                {
                    lblProcessStatusTxt.Text = "同單位已有申請";
                }
                System.Web.UI.WebControls.Label lblSendDate = e.Row.FindControl("lblSendDate") as Label;
                if (lblSendDate.Text != "")
                {
                    lblSendDate.Text = (Convert.ToDateTime(lblSendDate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(lblSendDate.Text).ToString("MM/dd");
                }
                


                System.Web.UI.WebControls.Label lblCreateDate = e.Row.FindControl("lblCreateDate") as Label;
                lblCreateDate.Text = (Convert.ToDateTime(lblCreateDate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(lblCreateDate.Text).ToString("MM/dd HH:mm");
                
            }
        }

        protected void gvBearList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBearList.PageIndex = e.NewPageIndex;
            Query();
        }
    }
}
