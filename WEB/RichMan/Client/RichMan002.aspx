﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RichMan/Client/WebLayout.Master"
    AutoEventWireup="true" CodeBehind="RichMan002.aspx.cs" Inherits="WEB.RichMan.Client.RichMan002" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        span.title
        {
            font-size: 16pt;
            color: Purple;
            font-weight: bold;
        }
        span.content
        {
            font-size: 10pt;
            margin-left: 20pt;
        }
        span.content-sub
        {
            font-size: 10pt;
            margin-left: 40pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        $(document).ready(function() {
            $("#menu a:eq(3)").addClass("on");
        })
    </script>

    <div class="wrap">
        <div class="content content2">
            <h1>
                DIY土石流防災大富翁
            </h1>
            <p style="font-size: 1.1em; font-weight: bold">
                DIY土石流防災大富翁有彩色稿及黑白稿兩種式樣，各類別檔案已製作成A4可印製之JPG檔，可自行選擇下載列印。說明如下。
            </p>
            <h3>
                步驟一﹕列印大富翁底圖，並裁切、黏貼；底圖共4張。</h3>
            <ul class="download">
                <li>【彩色列印】：<span><a href="download/全部地圖(彩色版).rar" target="_blank" title="全部地圖(彩色版)">下載全部</a></span></li>
                <li>【黑白列印】：<span><a href="download/全部地圖(黑白版).rar" target="_blank" title="全部地圖(黑白版)">下載全部</a></span></li>
            </ul>
            <table class="tableImg">
                <tr>
                    <td>
                        <img src="images/fortune6_map_DIY1.jpg" />
                    </td>
                    <td>
                        <img src="images/fortune6_map_DIY2.jpg" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="images/fortune6_map_DIY3.jpg" />
                    </td>
                    <td>
                        <img src="images/fortune6_map_DIY4.jpg" />
                    </td>
                </tr>
            </table>
            <h3>
                步驟二﹕列印遊戲卡片</h3>
            <p>
                <span>狀況卡</span> -共25張&nbsp; <span>防禦卡</span> -共25張&nbsp; <span>特殊卡</span> -共20張</p>
            <p>
                <span>錢幣</span> -100 元*50張、500元*20張、1000元*100張&nbsp; <span>土地所有權狀</span>-共19張，裁切完成，對折黏貼。
            </p>
            <ul class="download">
                <li>【彩色列印】：
                <span><a href="download/遊戲卡1.jpg" target="_blank" title="遊戲卡1">遊戲卡1</a></span>
                <span><a href="download/遊戲卡2.jpg" target="_blank" title="遊戲卡2">遊戲卡2</a></span>
                <span><a href="download/遊戲卡3.jpg" target="_blank" title="遊戲卡3">遊戲卡3</a></span>
                <span><a href="download/遊戲卡4.jpg" target="_blank" title="遊戲卡4">遊戲卡4</a></span>
                <span><a href="download/遊戲卡5.jpg" target="_blank" title="遊戲卡5">遊戲卡5</a></span>
                <span><a href="download/遊戲卡6.jpg" target="_blank" title="遊戲卡6">遊戲卡6</a></span>
                <span><a href="download/遊戲卡7.jpg" target="_blank" title="遊戲卡7">遊戲卡7</a></span>
                <span><a href="download/全部遊戲卡(彩色版).rar" target="_blank" title="全部遊戲卡(彩色版)">全部下載</a></span>
                </li>
                <li>【黑白列印】：
                <span><a href="download/黑白遊戲卡1.jpg" target="_blank" title="黑白遊戲卡1">遊戲卡1</a></span>
                <span><a href="download/黑白遊戲卡2.jpg" target="_blank" title="黑白遊戲卡2">遊戲卡2</a></span>
                <span><a href="download/黑白遊戲卡3.jpg" target="_blank" title="黑白遊戲卡3">遊戲卡3</a></span>
                <span><a href="download/黑白遊戲卡4.jpg" target="_blank" title="黑白遊戲卡4">遊戲卡4</a></span>
                <span><a href="download/黑白遊戲卡5.jpg" target="_blank" title="黑白遊戲卡5">遊戲卡5</a></span>
                <span><a href="download/黑白遊戲卡6.jpg" target="_blank" title="黑白遊戲卡6">遊戲卡6</a></span>
                <span><a href="download/黑白遊戲卡7.jpg" target="_blank" title="黑白遊戲卡7">遊戲卡7</a></span>
                <span><a href="download/全部遊戲卡(黑白版).rar" target="_blank" title="全部遊戲卡(黑白版)">全部下載</a></span>
                </li>
                <li>【彩色列印】：
                <span><a href="download/土地所有權狀1.jpg" target="_blank" title="權狀1">權狀1</a></span>
                <span><a href="download/土地所有權狀2.jpg" target="_blank" title="權狀2">權狀2</a></span>
                <span><a href="download/土地所有權狀3.jpg" target="_blank" title="權狀3">權狀3</a></span>
                <span><a href="download/土地所有權狀4.jpg" target="_blank" title="權狀4">權狀4</a></span>
                <span><a href="download/全部權狀卡(彩色版).rar" target="_blank" title="全部權狀卡(彩色版)">全部下載</a></span>
                </li>
                <li>【黑白列印】：
                <span><a href="download/黑白土地所有權狀1.jpg" target="_blank" title="權狀1">權狀1</a></span>
                <span><a href="download/黑白土地所有權狀2.jpg" target="_blank" title="權狀2">權狀2</a></span>
                <span><a href="download/黑白土地所有權狀3.jpg" target="_blank" title="權狀3">權狀3</a></span>
                <span><a href="download/黑白土地所有權狀4.jpg" target="_blank" title="權狀4">權狀4</a></span>
                <span><a href="download/全部權狀卡(黑白版).rar" target="_blank" title="全部權狀卡(黑白版)">全部下載</a></span>
                </li>
                <li>【彩色列印】：
                <span><a href="download/錢幣.jpg" target="_blank" title="錢幣">錢幣</a></span>
                <span><a href="download/錢幣.rar" target="_blank" title="錢幣(下載)">下載</a></span> 自行列印上述各幣值所需張數
                </li>
                <li>【黑白列印】：
                <span><a href="download/黑白錢幣.jpg" target="_blank" title="黑白錢幣">錢幣</a></span>
                <span><a href="download/黑白錢幣.rar" target="_blank" title="黑白錢幣(下載)">下載</a></span> 自行列印上述各幣值所需張數
                </li>
            </ul>
            <table style="text-align:center" class="tableImg2">
            <tr>
                <td><img src="images/fortune6_status-Card_1.jpg" alt="狀況卡" /></td>
                <td><img src="images/fortune6_defense -Card_1.jpg" alt="防禦卡" /></td>
            </tr>
            <tr>
                <td><img src="images/fortune6_angel-Card_1.jpg" alt="小天使卡" /></td>
                <td><img src="images/fortune6_money-Card_1000.jpg" alt="大富翁銀行" /></td>
            
            </tr>
            <tr>
                <td colspan="2"><img src="images/fortune6_status-Card_2.jpg" /></td>
            </tr>
            </table>
            <h3>
                步驟三﹕列印DIY骰子，延著邊線裁切，並黏貼成一個正方形。</h3>
                <p>列印DIY棋子，延著紅色實線裁切，虛線為折線，折成一個柱狀並黏貼好。</p>
                <ul class="download">
                <li>【彩色列印】：
                <span><a href="download/骰子_棋子.jpg" target="_blank" title="骰子+棋子">骰子+棋子</a></span>
                <span><a href="download/骰子_棋子.rar" target="_blank" title="骰子+棋子(下載)">下載</a></span> 自行列印上述各幣值所需張數
                </li>
                <li>【黑白列印】：
                <span><a href="download/黑白骰子_棋子.jpg" target="_blank" title="黑白骰子+棋子">骰子+棋子</a></span>
                <span><a href="download/黑白骰子_棋子.rar" target="_blank" title="黑白骰子+棋子(下載)">下載</a></span> 自行列印上述各幣值所需張數
                </li>
                </ul>
                <table>
                <tr>
                    <td>
                        <img src="images/fortune6_dice-Picture_1.jpg" /></td>
                    <td>
                        <img src="images/fortune6_dice-Picture_2.jpg" /></td>
                </tr>
                </table>
            <h3>
                步驟四﹕列印規則紙張</h3>
            <ul class="download">
                <li>【列印】：
                <span><a href="download/遊戲規則_small.jpg" target="_blank" title="遊戲規則">遊戲規則</a></span>
                <span><a href="download/遊戲規則_small.rar" target="_blank" title="遊戲規則(下載)">下載</a></span>
                </li>
                </ul>
                <p><img src="images/fortune6_rule-Picture.jpg" />
            </p>
            <h3>上述步驟都完成，即可開始進行防災大富翁遊戲</h3>
        </div>
    </div>
    <!-- wrap -->
</asp:Content>
