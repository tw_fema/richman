﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RichMan/Client/WebLayout.Master" AutoEventWireup="true" CodeBehind="RichMan003.aspx.cs" Inherits="WEB.RichMan.Client.RichMan003" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager2" runat="server">
        <Scripts>
            
        </Scripts>
    </asp:ScriptManager>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#menu a:eq(1)").addClass("on");
        })
    </script>
    
<!-- 申請名單 -->   
<div class="wrap">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" 
        UpdateMode="Conditional" >
    <ContentTemplate>
    
    <div>
        <span style="margin-right:5px">申請年度:</span><asp:DropDownList AutoPostBack="true" ID="ddlYear" runat="server" 
            onselectedindexchanged="ddlYear_SelectedIndexChanged" 
            DataTextField="createDatetest" DataValueField="createDatetest"></asp:DropDownList>
    </div>

    <asp:GridView ID="gvBearList" runat="server" AutoGenerateColumns="False" EmptyDataText="目前無資料"
            Caption="土石流防災大富翁明細資料" PagerSettings-Visible="true" CssClass="TableView"
                        AllowPaging="True" PageSize="15" 
        onrowdatabound="gvBearList_RowDataBound" 
            onpageindexchanging="gvBearList_PageIndexChanging" >
                        <Columns>
                            <asp:templatefield HeaderText="項次" ItemStyle-Width="5%" >
                                <itemtemplate>
                                    <%# Container.DataItemIndex + 1%>
                                </itemtemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:templatefield>
                            <asp:TemplateField HeaderText="服務單位"  HeaderStyle-HorizontalAlign ="Center" ItemStyle-Width="15%" >
                                <ItemTemplate>
                                    <asp:Label ID="lblSchool" runat="server" Text='<%# Eval("School") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                                                        
                            <asp:TemplateField HeaderText="姓名" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                    <asp:HiddenField ID="hiddenID" runat="server" Value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="申請數量" ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="lblRequireAmount" runat="server" Text='<%# Eval("RequireAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="推廣對象" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblRank" runat="server" Text='<%# Eval("Rank") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="教學用途" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lblCourseContent" runat="server" Text='<%# Eval("CourseContent") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="處理狀態" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblProcessStatusTxt" runat="server" Text='<%# Eval("ProcessStatusTxt") %>'></asp:Label>
                                    <asp:HiddenField ID="hiddenSendDate" runat="server" Value='<%# Eval("SendDate") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="申請時間" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="寄送時間" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblSendDate" runat="server" Text='<%# Eval("SendDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            
                            
                            
                           
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
            <HeaderStyle BackColor="#EEE9BF" Font-Bold="True" ForeColor="Black" />
            <AlternatingRowStyle BackColor="White" />
            <PagerSettings  FirstPageText="[First]" LastPageText="[Last]" NextPageText="Next" PageButtonCount="5" PreviousPageText="Previous" />
        </asp:GridView>
</ContentTemplate>
</asp:UpdatePanel>
</div><!-- wrap -->


</asp:Content>
