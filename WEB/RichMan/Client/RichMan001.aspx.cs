﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
//using Entity.bear;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB.RichMan.Client
{
    public partial class RichMan001 : System.Web.UI.Page
    {


        static int static_totalAmount;
        static int static_requireAmount;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                setddlTotal();
                setddlLevel();
                setddl3Year();
                setddl3Month();
                setddl3Day();

                int nowDay = DateTime.Now.Day;
                ddl3Day.SelectedValue = nowDay.ToString();


                //當庫存量小於申請數量不開放申請
                DataTable dt = Entity.RichMan.RichMan.GetALLBearList();
                //DataTable dt = new DataTable();
                //庫存
                int? totalAmount =  dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") =="已收入").Sum(obj => obj.Field<int?>("RequireAmount"));
                if (totalAmount != null)
                    totalAmount = Convert.ToInt32(totalAmount);
                
                //全部申請數量
                //var objs = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") != "已收入");

                int? StatusZero_RequireAmount = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "待處理").Sum(obj => obj.Field<int?>("RequireAmount"));
                int? StatusOne_RequireAmount = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "處理中").Sum(obj => obj.Field<int?>("RequireAmount"));
                int? StatusTwo_RequireAmount = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已送出").Sum(obj => obj.Field<int?>("SendAmount"));

                if (StatusZero_RequireAmount != null)
                    StatusZero_RequireAmount = Convert.ToInt32(StatusZero_RequireAmount);

                if (StatusOne_RequireAmount != null)
                    StatusOne_RequireAmount = Convert.ToInt32(StatusOne_RequireAmount);

                if (StatusTwo_RequireAmount != null)
                    StatusTwo_RequireAmount = Convert.ToInt32(StatusTwo_RequireAmount);

                if (totalAmount <= (StatusZero_RequireAmount + StatusOne_RequireAmount + StatusTwo_RequireAmount))
                {
                    pnlApply.Visible = false;
                    lblAlert.Text = "目前已無庫存量，暫不開放申請";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Js_alert", "javascript:alert('目前已無庫存量，暫不開放申請');", true);

                }

                static_totalAmount = totalAmount != null ? Convert.ToInt32(totalAmount) : 0;
                static_requireAmount = (StatusZero_RequireAmount != null ? Convert.ToInt32(StatusZero_RequireAmount) : 0) + (StatusOne_RequireAmount != null ? Convert.ToInt32(StatusOne_RequireAmount) : 0) + (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);    

                     
                
            }
            
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {

            
            if (checkForm())
            {
                
            

                //先看看庫存量夠不夠讓申請條件通過
                if ((static_totalAmount - (static_requireAmount + Convert.ToInt32(ddlAmount.SelectedValue))) < 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", "javascript:alert('庫存僅夠申請" + (static_totalAmount - static_requireAmount) + "份');", true);
                    return;
                }

                Entity.RichMan.RichMan obj = new Entity.RichMan.RichMan();
                setObj(obj);
                Entity.RichMan.RichMan.SaveClientObj(obj);


                

                string content = "";
                content += txt5Name.Text+"您好：<br><br>您已成功申請[土石流大富翁]";
                content += "<br><br><a href='http://246.swcb.gov.tw/richMan/richMan/client/RichMan003.aspx'>相關申請名單及處理進度資訊連結</a>";
                content += "<br><br>以下為您填寫的相關資料：;";
                content += "<br><br>1.  申請土石流大富翁 " + ddlAmount.SelectedValue + " 份";
                content += "<br>    2.  土石流大富翁推廣對象？小學" + ddlLevel.SelectedValue + "年級學生，或其他" + txtLevel.Text;
                content += "<br>    3.  預計何時安排土石流大富翁輔助教學？" + (obj.PlanDate.Year - 1911).ToString()+"/" + obj.PlanDate.ToString("MM/dd");
                content += "<br>    5.  個人聯繫資料：";
                content += "<br>　　　　服務單位：" +txt5School.Text;
                content += "<br>　　　　姓　　名：" + txt5Name.Text;
                content += "<br>　　　　職　　稱：" + txt5Job.Text;
                content += "<br>　　　　聯絡電話：" +txt5Tel.Text;
                content += "<br>　　　　電子郵件：" + txt5Email.Text;
                content += "<br>    6.  其他：" + txt6Ect.Text;
                content += "<br><br><br>土石流防災資訊網 敬上";

 

                
                //mail通知水保局負責人員
                MailMessage mailobj = new MailMessage();
                mailobj.From = new MailAddress("swcbfema@mail.swcb.gov.tw");
                mailobj.To.Add(txt5Email.Text.ToString());//申請人
                
                mailobj.Subject = "使用者完成申請「土石流大富翁」系統通知信";
                //mailobj.Body = "您好<br><br>您已成功申請[土石流大富翁]<br><a href='http://246.swcb.gov.tw/richMan/richMan/client/RichMan003.aspx'>相關申請名單及處理進度資訊連結</a><br><br>以下為您填寫的相關資料：<br><br>1.  申請土石流防災大富翁盒裝教材" + ddlAmount.SelectedValue + "份<br>    2.  土石流防災大富翁推廣對象？小學" + txt5School.Text + "年級學生，或其他" + txtLevel.Text + "<br>    4.  將運用於何種學科或課程，作為教學輔助教材?"+txt4CourseContent+"-----此為系統自動發送, 請勿直接回覆-----";

                mailobj.Body = content;
                mailobj.IsBodyHtml = true;
                mailobj.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                mailobj.Priority = System.Net.Mail.MailPriority.High;

                //要複本、密件副本 請放到 MailMessage.Body 之下
                mailobj.Bcc.Add("ycliao@fcu.edu.tw");//沛含
                mailobj.Bcc.Add("willis@mail.swcb.gov.tw");//力行
                //mailobj.Bcc.Add("lu24026@mail.swcb.gov.tw");//文俊
                mailobj.Bcc.Add("tidauna@mail.swcb.gov.tw");//宏洋

                // ***** 寄信才開啟 *****
                
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTP"];
                smtp.Port = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPPORT"]);
                if (smtp.Host == "210.69.127.5")
                    smtp.Credentials = new System.Net.NetworkCredential("swcbfema@mail.swcb.gov.tw", "_swcbfema!");
                smtp.Send(mailobj);
                




                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", "javascript:alert('資料已經送出,處理結果將回覆至" + obj.Email + "信箱。');location.href='RichMan003.aspx';", true);
            }
            
        }

        void setddlTotal()
        {
            //開放申請30份
            ddlAmount.Items.Add(new ListItem("請選擇", ""));
            for (int i = 1; i <= 30; i++)
            {
                ddlAmount.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }


        void setObj(Entity.RichMan.RichMan obj)
        {    
            
            obj.Name = txt5Name.Text;
            obj.School = txt5School.Text;
            obj.Address = txt5Address.Text;
            obj.CourseContent = txt4CourseContent.Text;
            obj.CreateDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            obj.ect = txt6Ect.Text;
            obj.Email = txt5Email.Text;
            obj.Job = txt5Job.Text;
            string Plandate = ddl3Year.SelectedValue + "/" + ddl3Mouth.SelectedValue + "/" + ddl3Day.SelectedValue;
            obj.PlanDate = Convert.ToDateTime(Plandate);
            if (txtLevel.Text != "")
                obj.Rank = txtLevel.Text;
            else
                obj.Rank = ddlLevel.SelectedValue;

            //0:待處理
            obj.ProcessStatus = 0;
            obj.RequireAmount = Convert.ToInt32(ddlAmount.SelectedValue);
            obj.Tel = txt5Tel.Text;

            //0:用戶端
            obj.ViewStatus = 0;
             
            
        }

        void setddlLevel()
        {
            ddlLevel.Items.Add(new ListItem("1", "一年級"));
            ddlLevel.Items.Add(new ListItem("2", "二年級"));
            ddlLevel.Items.Add(new ListItem("3", "三年級"));
            ddlLevel.Items.Add(new ListItem("4", "四年級"));
            ddlLevel.Items.Add(new ListItem("5", "五年級"));
            ddlLevel.Items.Add(new ListItem("6", "六年級"));

        }

        void setddl3Month()
        {
            if (ddl3Mouth.Items.Count != 0)
                ddl3Mouth.Items.Clear();

            int nowMonth = DateTime.Now.Month;
            for (int i = 1; i <= 12; i++)
            {
                ddl3Mouth.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddl3Mouth.SelectedValue = nowMonth.ToString();

        }
        void setddl3Day()
        {
            if (ddl3Day.Items.Count != 0)
                ddl3Day.Items.Clear();
            //int nowDay = DateTime.Now.Day;
            switch (Convert.ToInt32(ddl3Mouth.SelectedValue))
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    for (int i = 1; i <= 31; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 2:
                    for (int i = 1; i <= 28; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    for (int i = 1; i <= 30; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
            }
            //ddl3Day.SelectedValue = nowDay.ToString();
            
        }
        void setddl3Year()
        {
            int nowYear = DateTime.Now.Year;
            ddl3Year.Items.Add(new ListItem(nowYear.ToString(), nowYear.ToString()));
            ddl3Year.Items.Add(new ListItem((nowYear + 1).ToString(), (nowYear + 1).ToString()));
            ddl3Year.Items.Add(new ListItem((nowYear + 2).ToString(), (nowYear + 2).ToString()));            
        }

        bool checkForm()
        {
            //string tmp = "";
            if (ddlAmount.SelectedValue == "")
            {
                //tmp = tmp + ",份數不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請選擇份數!!');$('#ctl00_ContentPlaceHolder1_" + ddlAmount.ID + "').focus();", true);
                return false;
            }
            if (txt5School.Text == "")
            {
                //tmp = tmp + ",學校不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入服務單位!!');$('#ctl00_ContentPlaceHolder1_" + txt5School.ID + "').focus();", true);
                return false;
            }
            if (txt5Name.Text == "")
            {
                //tmp = tmp + ",姓名不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入姓名!!');$('#ctl00_ContentPlaceHolder1_" + txt5Name.ID + "').focus();", true);
                return false;
            }
            if (txt5Job.Text == "")
            {
                //tmp = tmp + ",職稱不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入職稱!!');$('#ctl00_ContentPlaceHolder1_" + txt5Job.ID + "').focus();", true);
                return false;
            }
            if (txt5Tel.Text == "")
            {

                //tmp = tmp + ",電話不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入電話!!');$('#ctl00_ContentPlaceHolder1_" + txt5Tel.ID + "').focus();", true);
                return false;
            }
            else
            {
                var regex = new Regex("[0-9]{2,3}-[0-9]{6,8}#{0,1}[0-9]{0,4}$");
                if (!regex.IsMatch(txt5Tel.Text))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('電話格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txt5Tel.ID + "').focus();", true);
                    return false;
                }
            }
            if (txt5Email.Text == "")
            {
                //tmp = tmp + ",Email不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入Email!!');$('#ctl00_ContentPlaceHolder1_" + txt5Email.ID + "').focus();", true);
                return false;
            }
            if (!CheckEmail(txt5Email.Text))
            {
                //tmp = tmp + "Email 格式錯誤";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('Email 格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txt5Email.ID + "').focus();", true);
                return false;
            }
            if (txt5Address.Text == "")
            {
                //tmp = tmp + ",地址不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入地址!!');$('#ctl00_ContentPlaceHolder1_" + txt5Address.ID + "').focus();", true);
                return false;
            }
            
            
            return true; 
        }

        protected void ddl3Year_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddl3Mouth_SelectedIndexChanged(object sender, EventArgs e)
        {
            setddl3Day();
            UpdatePanel1.Update();
        }

        private bool CheckEmail(string EmailAddress)
        {

            string strPattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            if (System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, strPattern))
            { return true; }
            return false;
        }

        
    }
}
