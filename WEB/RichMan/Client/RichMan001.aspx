﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RichMan/Client/WebLayout.Master" AutoEventWireup="true" CodeBehind="RichMan001.aspx.cs" Inherits="WEB.RichMan.Client.RichMan001" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            
        </Scripts>
    </asp:ScriptManager>
    
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#menu a:eq(0)").addClass("on");
        })
    </script>

<div class="content">
<h1>「土石流防災大富翁」申請須知</h1>
    <img src="images/大型土石流防災大富翁宣導版.jpg" align="right" alt="大型土石流防災大富翁宣導版" />
<h2>一、大型土石流防災大富翁(活動宣導用) </h2>
<p>本局為推廣水土保持及土石流防災教育，製作土石流防災大富翁大型遊戲底圖，尺寸為4公尺×2.5公尺，並免費提供電子圖檔，如欲自行印製用於舉辦相關教育宣導活動者，請電洽(049-23475142 郭力行工程員) </p>
<h2>二、土石流防災大富翁盒裝版 </h2>
<ol >
  <li>索取條件： 為推廣水土保持及土石流防災教育，本局提供土石流防災大富翁盒裝版免費索取，可作為自然科教學輔助教材，歡迎教育推廣或教學單位踴躍索取；因數量有限，每個單位一次最多申請數量限30份，贈完為止！ </li>
  <li>申請方式： 請直接於本網站「線上申請」登記申請單位、需求份數等相關資料，本局將統一辦理郵寄程序。 </li>
  <!--<li>若運用土石流大富翁作為環境教育輔助教材，敬請將推廣經驗及教學心得連同照片上傳至本網站「意見分享」，本局將致贈精美小禮物一份；針對本教材若發現任何問題或有任何建議，亦請不吝指教，謝謝！ </li>-->
  <li>運用土石流防災大富翁遊戲作為防災教育推廣輔助教材，針對本教材若發現任何問題或有任何建議，亦請不吝指教，謝謝！ </li>
</ol>

<div style="margin-left: 4em;">
    <br />
    <img class="none" src="images/土石流防災大富翁盒裝印刷版.jpg" alt="土石流防災大富翁盒裝印刷版" /><br /><br />
    <asp:Label ID="lblAlert" style="color:Red;font-size:24pt" runat="server"></asp:Label>
</div> 

</div><!-- wrap -->   


<asp:Panel ID="pnlApply" runat="server">

<div class="content">
<h1>線上申請</h1>   
</div>
<div class="wrap">
    <div >
        <strong style="color:Red">＊為必填欄位</strong>
        <br/><br/>
        <ul>
        <li>
            <strong>1. 申請土石流防災大富翁盒裝教材</strong>
            <p><span style="color:Red; font-size:18pt">＊</span>
            <asp:DropDownList runat="server" ID="ddlAmount" ></asp:DropDownList>
            份</p>
        </li>

        <li>
            <strong>2. 土石流防災大富翁推廣對象？</strong><p class="indent">
            小學
            <asp:DropDownList runat="server" ID="ddlLevel" ></asp:DropDownList>
            年級學生，或其他：<asp:TextBox ID="txtLevel" runat="server"></asp:TextBox></p>
        </li>

        <li>
            <strong>3. 預計何時安排土石流防災大富翁輔助教學？</strong>
            <div style="margin-left:15px">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:DropDownList runat="server" ID="ddl3Year" 
                    onselectedindexchanged="ddl3Year_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddl3Mouth" 
                    onselectedindexchanged="ddl3Mouth_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddl3Day" ></asp:DropDownList>
            </ContentTemplate>
            
            </asp:UpdatePanel>
        </div>
        </li>
        
        <li>
            <strong>4. 將運用於何種學科或課程，作為教學輔助教材?</strong>
            
            <p class="indent">
            <asp:TextBox ID="txt4CourseContent" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox></p>
        </li>
        
        <li>
            <strong>5. 請填寫聯繫資料，便於後續教育推廣交流，謝謝！</strong>            
            <table>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>服務單位：</td>
                    <td><asp:TextBox ID="txt5School" runat="server"  ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>姓名：</td>
                    <td><asp:TextBox ID="txt5Name" runat="server" MaxLength="10" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>職稱：</td>
                    <td><asp:TextBox ID="txt5Job" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>聯絡電話：</td>
                    <td><asp:TextBox ID="txt5Tel" runat="server" ></asp:TextBox><span style="color:Red">※有分機電話格式：04-8825135#360</span></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>E-MAIL：</td>
                    <td><asp:TextBox ID="txt5Email" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>寄送地址：</td>
                    <td><asp:TextBox ID="txt5Address" runat="server" ></asp:TextBox></td>
                </tr>
            </table>
        </li>
        
        <li>
            <strong>6. 其他</strong><br/>
            <p class="indent"><asp:TextBox TextMode="MultiLine" ID="txt6Ect" Rows="5" runat="server"></asp:TextBox></p>
        </li>
    
    </ul>
    </div>
    
    <div class="btnBlock">
        <asp:Button ID="btnOK" runat="server"  Text="送出申請" Height="30px" Width="24em" onclick="btnOK_Click"/>
    </div>
    
</div><!-- wrap --> 


</asp:Panel>
   
</asp:Content>
