﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace WEB.RichMan.Server
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void btnLoginOK_Click(object sender, EventArgs e)
        {
            Entity.RichMan.User pUser = Entity.RichMan.User.Login(txtAccount.Text, txtPassword.Text);
            if (pUser == null)
            {
                this.ClientScript.RegisterStartupScript(this.GetType(), "error", "<script>alert('帳號密碼錯誤');</script>", false);
                return;
            }

            //清除 cookies 
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }

            FormsAuthentication.SetAuthCookie(pUser.Account, false);

            //清除舊的 Cookies，Cookies的存活時間設為一週
            Response.Cookies["Account"].Value = pUser.Account;
            Response.Cookies["Account"].Expires = DateTime.Now.AddDays(7);

            Response.Cookies["UID"].Value = pUser.Uid;
            Response.Cookies["UID"].Expires = DateTime.Now.AddDays(7);


            Response.Cookies["Domain"].Value = "";
            Response.Cookies["Domain"].Expires = DateTime.Now.AddDays(7);

            //LogonController.SaveLoginLog(pUser.Account, Request.ServerVariables["REMOTE_ADDR"]);


            //Response.Cookies["IsAdmin"].Value = "False";
            //Response.Cookies["IsAdmin"].Expires = DateTime.Now.AddDays(7);
            //Response.Redirect("~/bear/Server/bearOrderList.aspx");
            Response.Redirect("~/RichMan/server/RichManOrderList.aspx");



        }
    }
}
