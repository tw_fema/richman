﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/RichMan/Server/WebLayout.Master" CodeBehind="RichManOrderListEdit.aspx.cs" Inherits="WEB.RichMan.Server.RichManOrderListEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript">
    $(document).ready(function() {
    $("a.titleMenu").eq(0).addClass("on");
    });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!------------------------updatePanel  用-------------------------------->
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            
        </Scripts>
    </asp:ScriptManager>
    <div><ol>
        <li>
            <strong>申請土石流防災大富翁教材</strong><br/>
            <asp:DropDownList runat="server" ID="ddlAmount"  Enabled="false" ></asp:DropDownList>
            份
        </li>

        <li>
            <strong>土石流防災大富翁推廣對象？</strong><br/>
            小學
            <asp:DropDownList runat="server" ID="ddlLevel" Enabled="false" ></asp:DropDownList>
            年級學生，或其他：<asp:TextBox ID="txtLevel" Enabled="false" runat="server"></asp:TextBox>
        </li>

        <li>
            <strong>預計何時安排土石流防災大富翁輔助教學？</strong><br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:DropDownList runat="server" ID="ddl3Year"  Enabled="false"
                    onselectedindexchanged="ddl3Year_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddl3Mouth" Enabled="false" 
                    onselectedindexchanged="ddl3Mouth_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList runat="server" ID="ddl3Day" Enabled="false" ></asp:DropDownList>
            </ContentTemplate>
            </asp:UpdatePanel>
        
        </li>
        
        <li>
            <strong>將運用於何種學科或課程，作為教學輔助教材?</strong>
            
            <br />
            <asp:TextBox ID="txt4CourseContent" Enabled="false" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
        </li>
        
        <li>
            <strong>請填寫聯繫資料，便於後續教育推廣交流，謝謝！</strong>            
            <table>
                <tr>
                    <td>服務單位：</td>
                    <td><asp:TextBox ID="txt5School" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>姓名：</td>
                    <td><asp:TextBox ID="txt5Name" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>職稱：</td>
                    <td><asp:TextBox ID="txt5Job" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>聯絡電話：</td>
                    <td><asp:TextBox ID="txt5Tel" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>E-MAIL：</td>
                    <td><asp:TextBox ID="txt5Email" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>寄送地址：</td>
                    <td><asp:TextBox ID="txt5Address" Enabled="false" runat="server" ></asp:TextBox></td>
                </tr>
            </table>
        </li>
        
        <li>
            <strong>其他</strong><br/>
            <asp:TextBox TextMode="MultiLine" ID="txt6Ect" Enabled="false" Rows="5" runat="server"></asp:TextBox>
        </li>
    
    </ul></div>
    <hr />
    <div>
    <p style ="color:Red">當處理進度為：已送出，才會儲存實際寄送時間和實際寄送數量</p>
    </div>
    <div>
        <ul>
        <li>
            <strong>實際寄送數量</strong><br/>
            <asp:TextBox ID="txtSendAmount" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" runat="server" ></asp:TextBox>
            <br />
        </li>
        <li>
            <strong>實際寄送時間</strong>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:DropDownList runat="server" ID="ddlSendDateYear" 
                     ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddlSendDateMonth" onselectedindexchanged="ddlSendDateMonth_SelectedIndexChanged" 
                     ></asp:DropDownList> / 
                <asp:DropDownList runat="server" ID="ddlSendDateDay" ></asp:DropDownList>
            </ContentTemplate>
            </asp:UpdatePanel>
            
            <br />
            
        </li>
        <li>
            <strong>送貨編號</strong>
            <asp:TextBox ID="txtItemNumber" runat="server" ></asp:TextBox>
        </li>
        <li>
            <strong>處理進度</strong>
            
            <br />
            <asp:DropDownList  runat="server" ID="ddlProcessStatus" ></asp:DropDownList>
        </li>
        </ol>
        <asp:HiddenField ID="hiddenViewStatus" runat="server" />
    </div>
    <div style="text-align:center">
        <asp:Button ID="btnOK" runat="server" Text="儲存" onclick="btnOK_Click" />
    </div>
</asp:Content>
    
    

