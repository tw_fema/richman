﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace WEB.RichMan.Server
{
    public partial class RichManOrderListEdit : System.Web.UI.Page
    {
        Entity.RichMan.RichMan objBear;

        //所有倉庫進貨量
        static int static_totalAmount;

        //目前申請總量：
        static int static_requireAmount;

        //實際寄送數量
        static int static_realAmount;

        //原本的寄送數量
        static int oldSendAmount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies["UID"];
            if (cookie == null)
            {
                Response.Redirect("~/RichMan/Server/login.aspx");
            }

            string uid = Request.Cookies["UID"].Value;
            if (uid == null || uid == "")
                Response.Redirect("~/RichMan/Server/login.aspx");
            if (!IsPostBack)
            {

                
                setddlProcessStatus();
                setddlLevel();
                setddl3Year();
                setddl3Month();
                setddl3Day();

                setddlSendDateYear();
                setddlSendDateMonth();
                setddlSendDateDay();
                int nowDay = DateTime.Now.Day;
                ddlSendDateDay.SelectedValue = nowDay.ToString();
                


                string id = Request.QueryString["id"].ToString();
                DataTable dt = Entity.RichMan.RichMan.GetOneBearYearList(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    txt5Name.Text = dt.Rows[0]["Name"].ToString();
                    txt5School.Text = dt.Rows[0]["School"].ToString();
                    txt5Address.Text = dt.Rows[0]["Address"].ToString();
                    txt5Email.Text = dt.Rows[0]["Email"].ToString();
                    txt5Job.Text = dt.Rows[0]["Job"].ToString();
                    txt4CourseContent.Text = dt.Rows[0]["CourseContent"].ToString();
                    txt5Tel.Text = dt.Rows[0]["Tel"].ToString();
                    txt6Ect.Text = dt.Rows[0]["Ect"].ToString();
                    txtLevel.Text = dt.Rows[0]["Rank"].ToString();
                    txtSendAmount.Text = dt.Rows[0]["SendAmount"].ToString();
                    oldSendAmount = txtSendAmount.Text != "" && txtSendAmount.Text != "0" ? Convert.ToInt32(txtSendAmount.Text) : 0;
                    txtItemNumber.Text = dt.Rows[0]["ItemNumber"].ToString();

                    hiddenViewStatus.Value = dt.Rows[0]["ViewStatus"].ToString();



                    

                    ddl3Year.SelectedValue = Convert.ToDateTime(dt.Rows[0]["planDate"]).Year.ToString();
                    ddl3Mouth.SelectedValue = Convert.ToDateTime(dt.Rows[0]["planDate"]).Month.ToString();
                    ddl3Day.SelectedValue = Convert.ToDateTime(dt.Rows[0]["planDate"]).Day.ToString();

                    ddlProcessStatus.SelectedValue = dt.Rows[0]["ProcessStatus"].ToString();

                    if (dt.Rows[0]["sendDate"] != DBNull.Value)
                    {
                        ddlSendDateYear.SelectedValue = Convert.ToDateTime(dt.Rows[0]["sendDate"]).Year.ToString();
                        ddlSendDateMonth.SelectedValue = Convert.ToDateTime(dt.Rows[0]["sendDate"]).Month.ToString();
                        ddlSendDateDay.SelectedValue = Convert.ToDateTime(dt.Rows[0]["sendDate"]).Day.ToString();
                    }

                    setddlTotal(Convert.ToInt32(dt.Rows[0]["viewStatus"]));
                    ddlAmount.SelectedValue = dt.Rows[0]["requireAmount"].ToString();


                }


                //當庫存量小於申請數量不開放申請
                DataTable dtTotal = Entity.RichMan.RichMan.GetALLBearList();
                //庫存
                int? totalAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已收入").Sum(obj => obj.Field<int?>("RequireAmount"));
                if (totalAmount != null)
                {
                    totalAmount = Convert.ToInt32(totalAmount);
                }

                //全部申請數量
                //var objs = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") != "已收入");

                int? StatusZero_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "待處理").Sum(obj => obj.Field<int?>("RequireAmount"));
                int? StatusOne_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "處理中").Sum(obj => obj.Field<int?>("RequireAmount"));
                int? StatusTwo_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已送出").Sum(obj => obj.Field<int?>("SendAmount"));

                if (StatusZero_RequireAmount != null)
                    StatusZero_RequireAmount = Convert.ToInt32(StatusZero_RequireAmount);

                if (StatusOne_RequireAmount != null)
                    StatusOne_RequireAmount = Convert.ToInt32(StatusOne_RequireAmount);

                if (StatusTwo_RequireAmount != null)
                    StatusTwo_RequireAmount = Convert.ToInt32(StatusTwo_RequireAmount);


                //所有倉庫進貨量
                static_totalAmount = totalAmount != null ? Convert.ToInt32(totalAmount) : 0;

                //目前申請總量：
                static_requireAmount = (StatusZero_RequireAmount != null ? Convert.ToInt32(StatusZero_RequireAmount) : 0) + (StatusOne_RequireAmount != null ? Convert.ToInt32(StatusOne_RequireAmount) : 0) + (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);

                //實際寄送數量
                static_realAmount = (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);
                
            }
        }

        

        void setddlTotal(int ViewStatus)
        {
            if (ViewStatus == 0)
            {
                //開放申請10份
                ddlAmount.Items.Add(new ListItem("請選擇", ""));
                for (int i = 1; i <= 30; i++)
                {
                    ddlAmount.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            else if (ViewStatus == 1)
            {
                for (int i = 1; i <= 1000; i++)
                {
                    ddlAmount.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlAmount.Items.Add(new ListItem("請選擇", ""));
            }
        }

        void setddlProcessStatus()
        {
            if (ddlProcessStatus.Items.Count > 0)
                ddlProcessStatus.Items.Clear();
            ddlProcessStatus.Items.Add(new ListItem("待處理", "0"));
            ddlProcessStatus.Items.Add(new ListItem("處理中", "1"));
            ddlProcessStatus.Items.Add(new ListItem("已送出", "2"));
            ddlProcessStatus.Items.Add(new ListItem("重複申請", "3"));

        }

        /*
        void setObj(Entity.RichMan.RichMan obj)
        {
            obj.Name = txt5Name.Text;
            obj.School = txt5School.Text;
            obj.Address = txt5Address.Text;
            obj.CourseContent = txt4CourseContent.Text;
            obj.CreateDate = DateTime.Now;
            obj.ect = txt6Ect.Text;
            obj.Email = txt5Email.Text;
            obj.Job = txt5Job.Text;
            string Plandate = ddl3Year.SelectedValue + "/" + ddl3Mouth.SelectedValue + "/" + ddl3Day.SelectedValue;
            obj.PlanDate = Convert.ToDateTime(Plandate);
            if (txtLevel.Text != "")
                obj.Rank = txtLevel.Text;
            else
                obj.Rank = ddlLevel.SelectedValue;

            //0:待處理
            obj.ProcessStatus = 0;
            obj.RequireAmount = Convert.ToInt32(ddlAmount.SelectedValue);
            obj.Tel = txt5Tel.Text;

            obj.ViewStatus = Convert.ToInt32(hiddenViewStatus.Value);

        }
         */ 

        void setddlLevel()
        {
            ddlLevel.Items.Add(new ListItem("1", "一年級"));
            ddlLevel.Items.Add(new ListItem("2", "二年級"));
            ddlLevel.Items.Add(new ListItem("3", "三年級"));
            ddlLevel.Items.Add(new ListItem("4", "四年級"));
            ddlLevel.Items.Add(new ListItem("5", "五年級"));
            ddlLevel.Items.Add(new ListItem("6", "六年級"));

        }

        void setddl3Month()
        {
            if (ddl3Mouth.Items.Count != 0)
                ddl3Mouth.Items.Clear();

            int nowMonth = DateTime.Now.Month;
            for (int i = 1; i <= 12; i++)
            {
                ddl3Mouth.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddl3Mouth.SelectedValue = nowMonth.ToString();

        }
        void setddl3Day()
        {
            if (ddl3Day.Items.Count != 0)
                ddl3Day.Items.Clear();
            int nowDay = DateTime.Now.Day;
            switch (Convert.ToInt32(ddl3Mouth.SelectedValue))
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    for (int i = 1; i <= 31; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 2:
                    for (int i = 1; i <= 28; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    for (int i = 1; i <= 30; i++)
                    {
                        ddl3Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
            }
            ddl3Day.SelectedValue = nowDay.ToString();
        }

        void setddl3Year()
        {
            int nowYear = DateTime.Now.Year;
            ddl3Year.Items.Add(new ListItem(nowYear.ToString(), nowYear.ToString()));
            ddl3Year.Items.Add(new ListItem((nowYear + 1).ToString(), (nowYear + 1).ToString()));
            ddl3Year.Items.Add(new ListItem((nowYear + 2).ToString(), (nowYear + 2).ToString()));
        }

        void setddlSendDateYear()
        {
            int nowYear = DateTime.Now.Year;
            ddlSendDateYear.Items.Add(new ListItem(nowYear.ToString(), nowYear.ToString()));
            ddlSendDateYear.Items.Add(new ListItem((nowYear + 1).ToString(), (nowYear + 1).ToString()));
            ddlSendDateYear.Items.Add(new ListItem((nowYear + 2).ToString(), (nowYear + 2).ToString()));
        }

        void setddlSendDateMonth()
        {
            if (ddlSendDateMonth.Items.Count != 0)
                ddlSendDateMonth.Items.Clear();

            int nowMonth = DateTime.Now.Month;
            for (int i = 1; i <= 12; i++)
            {
                ddlSendDateMonth.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlSendDateMonth.SelectedValue = nowMonth.ToString();
        }

        void setddlSendDateDay()
        {
            if (ddlSendDateDay.Items.Count != 0)
                ddlSendDateDay.Items.Clear();
            //int nowDay = DateTime.Now.Day;
            switch (Convert.ToInt32(ddlSendDateMonth.SelectedValue))
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    for (int i = 1; i <= 31; i++)
                    {
                        ddlSendDateDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 2:
                    for (int i = 1; i <= 28; i++)
                    {
                        ddlSendDateDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    for (int i = 1; i <= 30; i++)
                    {
                        ddlSendDateDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    break;
            }
            //ddlSendDateDay.SelectedValue = nowDay.ToString();
            
        }

        private bool CheckEmail(string EmailAddress)
        {

            string strPattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            if (System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, strPattern))
            { return true; }
            return false;
        }

        protected void ddlSendDateMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            setddlSendDateDay();
            UpdatePanel2.Update();
        }

        protected void ddl3Mouth_SelectedIndexChanged(object sender, EventArgs e)
        {
            setddl3Day();
            UpdatePanel1.Update();
        }

        protected void ddl3Year_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected bool checkFrom()
        {
            if (txtSendAmount.Text == "")
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請填寫實際寄送數量!!');$('#ctl00_ContentPlaceHolder1_" + txtSendAmount.ID + "').focus();", true);
                return false;
            }
            else
            {
                var regex = new Regex("^[0-9]*[1-9][0-9]*$");
                if (!regex.IsMatch(txtSendAmount.Text))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('寄送份數格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txtSendAmount.ID + "').focus();", true);
                    return false;
                }
            }
            return true;
        }


        protected void btnOK_Click(object sender, EventArgs e)
        {
            //先看看庫存量夠不夠讓申請條件通過
            //實際申請數量
           
            if (ddlProcessStatus.SelectedItem.Text == "已送出")
            {



                //當已經有填實寄寄送數量，要扣回來
                if (txtSendAmount.Text != "")
                {
                    if ((static_realAmount + (txtSendAmount.Text != "" ? Convert.ToInt32(txtSendAmount.Text) : 0) > static_totalAmount + Convert.ToInt32(oldSendAmount)))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", "javascript:alert('庫存僅夠申請" + (static_totalAmount - static_realAmount) + "份，重填實際寄送數量');", true);
                        return;
                    }
                }
                //當實際寄送數量 + 此次實際寄送數量 > 所有倉庫進貨量
                else if ((static_realAmount + (txtSendAmount.Text != "" ? Convert.ToInt32(txtSendAmount.Text) : 0) > static_totalAmount))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", "javascript:alert('庫存僅夠申請" + (static_totalAmount - static_realAmount) + "份，重填實際寄送數量');", true);
                    return;
                }

                if (!checkFrom())
                {
                    return;
                }

            }



            Entity.RichMan.RichMan objBear = new Entity.RichMan.RichMan();
            setBearObj(objBear);
            Entity.RichMan.RichMan.UpdateServerObj(objBear, ddlProcessStatus.SelectedItem.Text);

            if (ddlProcessStatus.SelectedItem.Text == "已送出")
            {
                //mail通知申請人

                MailMessage mailobj = new MailMessage();
                mailobj.From = new MailAddress("willis@mail.swcb.gov.tw");
                mailobj.To.Add(txt5Email.Text.ToString());//申請人


                string content = "";
                content += txt5Name.Text + "，您好：<br><br>您所申請[土石流大富翁]";
                content += "<br><br>已於 " + (objBear.SendDate.Year - 1911).ToString() + "/" + objBear.SendDate.ToString("MM/dd") + "送出";
                content += "<br><br><a href='http://246.swcb.gov.tw/RichMan/RichMan/Client/RichMan003.aspx'>相關申請處理進度資訊連結</a>";
                content += "<br><br><br>土石流防災資訊網 敬上";


                mailobj.Subject = "「土石流大富翁」系統通知信(已送出)";
                //mailobj.Body = "您好<br><br>您透過246系統申請土石流大富翁,<br>系統管理員已審核之.<br>近期將寄到你所填的地址 <br><br>-----此為系統自動發送, 請勿直接回覆-----";
                mailobj.Body = content;
                mailobj.IsBodyHtml = true;
                mailobj.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                mailobj.Priority = System.Net.Mail.MailPriority.High;

                //要複本、密件副本 請放到 MailMessage.Body 之下
                mailobj.Bcc.Add("ycliao@fcu.edu.tw");//沛含
                mailobj.Bcc.Add("willis@mail.swcb.gov.tw");//力行
                //mailobj.Bcc.Add("lu24026@mail.swcb.gov.tw");//文俊
                mailobj.Bcc.Add("tidauna@mail.swcb.gov.tw");//宏洋


                // ***** 寄信再開啟 ******

                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTP"];
                smtp.Port = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPPORT"]);
                if (smtp.Host == "210.69.127.5")
                    smtp.Credentials = new System.Net.NetworkCredential("swcbfema@mail.swcb.gov.tw", "_swcbfema!");
                smtp.Send(mailobj);



            }
            else if (ddlProcessStatus.SelectedItem.Text == "重複申請")
            {
                //mail通知申請人

                MailMessage mailobj = new MailMessage();
                mailobj.From = new MailAddress("willis@mail.swcb.gov.tw");
                mailobj.To.Add(txt5Email.Text.ToString());//申請人


                string content = "";
                content += txt5Name.Text + "，您好：<br><br>您所申請[土石流大富翁]，同一服務單位已經申請過了。";
                content += "<br><br><a href='http://246.swcb.gov.tw/richMan/richMan/client/RichMan003.aspx'>相關申請處理進度資訊連結</a>";
                content += "<br><br><br>土石流防災資訊網 敬上";


                mailobj.Subject = "「土石流大富翁」系統通知信(重複申請)";
                //mailobj.Body = "您好<br><br>您透過246系統申請土石流大富翁,<br>系統管理員已審核之.<br>近期將寄到你所填的地址 <br><br>-----此為系統自動發送, 請勿直接回覆-----";
                mailobj.Body = content;
                mailobj.IsBodyHtml = true;
                mailobj.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                mailobj.Priority = System.Net.Mail.MailPriority.High;

                //要複本、密件副本 請放到 MailMessage.Body 之下
                mailobj.Bcc.Add("ycliao@fcu.edu.tw");//沛含
                mailobj.Bcc.Add("willis@mail.swcb.gov.tw");//力行
                //mailobj.Bcc.Add("lu24026@mail.swcb.gov.tw");//文俊
                mailobj.Bcc.Add("tidauna@mail.swcb.gov.tw");//宏洋


                // ***** 寄信再開啟 ******
                
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTP"];
                smtp.Port = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPPORT"]);
                if (smtp.Host == "210.69.127.5")
                    smtp.Credentials = new System.Net.NetworkCredential("swcbfema@mail.swcb.gov.tw", "_swcbfema!");
                smtp.Send(mailobj);
                  
            }

            Response.Redirect("~/RichMan/Server/RichManOrderList.aspx");
            
        }

        void setBearObj(Entity.RichMan.RichMan objBear)
        {


            objBear.ID = Convert.ToInt32(Request.QueryString["id"].ToString());
            objBear.Name = txt5Name.Text;
            objBear.School = txt5School.Text;
            objBear.Address = txt5Address.Text;
            objBear.Email = txt5Email.Text;
            objBear.Job = txt5Job.Text;
            objBear.CourseContent = txt4CourseContent.Text;
            objBear.Tel = txt5Tel.Text;
            objBear.ect = txt6Ect.Text;
            objBear.Rank = txtLevel.Text;
            objBear.RequireAmount = Convert.ToInt32(ddlAmount.SelectedValue);
            objBear.PlanDate = Convert.ToDateTime(ddl3Year.SelectedValue + "/" + ddl3Mouth.SelectedValue + "/" + ddl3Day.SelectedValue);
            if ( ddlProcessStatus.SelectedItem.Text == "已送出")
            {
                objBear.SendAmount = (txtSendAmount.Text != "") ? Convert.ToInt32(txtSendAmount.Text) : 0;
                objBear.SendDate = Convert.ToDateTime(ddlSendDateYear.SelectedValue + "/" + ddlSendDateMonth.SelectedValue + "/" + ddlSendDateDay.SelectedValue);
                objBear.ItemNumber = txtItemNumber.Text;
                
            }
            else if (ddlProcessStatus.SelectedItem.Text == "重複申請")
            {
                objBear.SendAmount = 0;
                objBear.ItemNumber = "";

            }
            objBear.ProcessStatus = Convert.ToInt32(ddlProcessStatus.SelectedValue);
            objBear.ViewStatus = Convert.ToInt32(hiddenViewStatus.Value);

                
        }
    }
}
