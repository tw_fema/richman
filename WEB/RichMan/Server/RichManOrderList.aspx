﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/RichMan/Server/WebLayout.Master" CodeBehind="RichManOrderList.aspx.cs" Inherits="WEB.RichMan.Server.RichManOrderList" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        $(document).ready(function() {
        $("a.titleMenu").eq(0).addClass("on");
        });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div>
        <div class="TxtStyle">
            <!--所有倉庫進貨量：--><asp:Label Visible="false" runat="server" ID="lblstatic_totalAmount" Text=""></asp:Label>
            庫存量：<asp:Label runat="server" ID="lblstatic_realAmount" Text=""></asp:Label>
            尚可申請數量：<asp:Label runat="server" ID="lblstatic_haveAmount" Text=""></asp:Label>
            待寄送數量：<asp:Label runat="server" ID="lblstatic_waitSendAmount" Text=""></asp:Label>
            <!--實際寄送數量：--><asp:Label Visible="false" runat="server" ID="lblstatic_sendAmount" Text=""></asp:Label>
            <!--目前申請總量：--><asp:Label Visible="false" runat="server" ID="lblstatic_requireAmount" Text=""></asp:Label>
        </div>
        <br/>
        <hr style="border-top:1px dotted #ccc;HEIGHT:0;">

        <div class="TxtStyle2">
        申請年度:<asp:DropDownList AutoPostBack="true" ID="ddlYear" runat="server" 
            onselectedindexchanged="ddlYear_SelectedIndexChanged1"></asp:DropDownList> &nbsp
        申請方式:<asp:DropDownList AutoPostBack="true" ID="ddlViewStatus" runat="server" onselectedindexchanged="ddlViewStatus_SelectedIndexChanged" 
            ></asp:DropDownList>&nbsp
        處理狀態:<asp:DropDownList AutoPostBack="true" ID="ddlProcessStatus" runat="server" onselectedindexchanged="ddlProcessStatus_SelectedIndexChanged"  
            ></asp:DropDownList>&nbsp
            
    </div>
        <asp:GridView ID="gvBearList" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="98%" 
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" 
            BorderWidth="1px" EmptyDataText="無資料"  DataKeyNames="ID"
                        AllowPaging="True" PageSize="10"
            onrowdatabound="gvBearList_RowDataBound" 
            onpageindexchanging="gvBearList_PageIndexChanging1" Font-Size="13px" >
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:TemplateField HeaderText="項次">
                                <itemtemplate>
                                    <%# Container.DataItemIndex + 1%>
                                    <asp:HiddenField ID="hiddenID" runat="server" Value='<%# Eval("ID") %>' />
                                </itemtemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="服務單位"  HeaderStyle-HorizontalAlign ="Center" >
                                <ItemTemplate>
                                    <asp:Label ID="lblSchool" runat="server" Text='<%# Eval("School") %>'></asp:Label>
                                </ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                                                        
                            <asp:TemplateField HeaderText="姓名">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="電話">
                                <ItemTemplate>
                                    <asp:Label ID="lblTel" runat="server" Text='<%# Eval("Tel") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="地址">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="申請時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="申請數量">
                                <ItemTemplate>
                                    <asp:Label ID="lblRequireAmount" runat="server" Text='<%# Eval("RequireAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            
                            
                            <asp:TemplateField HeaderText="實際寄送時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblSendDate" runat="server" Text='<%# Eval("SendDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="實際寄送數量">
                                <ItemTemplate>
                                    <asp:Label ID="lblSendAmount" runat="server" Text='<%# Eval("SendAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="送貨編號">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemNumber" runat="server" Text='<%# Eval("ItemNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="處理狀態">
                                <ItemTemplate>
                                    <asp:Label ID="lblProcessStatusTxt" runat="server" Text='<%# Eval("ProcessStatusTxt") %>'></asp:Label>
                                    <asp:HiddenField ID="hiddenProcessStatus" runat="server" Value='<%# Eval("ProcessStatus") %>' />
                                    <asp:HiddenField ID="hiddenSendDate" runat="server" Value='<%# Eval("SendDate") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>

                            
                            <asp:TemplateField HeaderText="功能">
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="lbtnView" runat="server" ImageUrl="~/images/icon-view.gif" ToolTip="檢視" />--%>
                                    
                                    <asp:HyperLink runat="server" ID="hyModify" ImageUrl="~/RichMan/Client/images/icon-edit.gif" NavigateUrl="~/RichMan/Server/RichManOrderListEdit.aspx"></asp:HyperLink>
                                    <asp:ImageButton ID="lbtnDelete" runat="server" ImageUrl="~/RichMan/Client/images/icon-del.png" ToolTip="刪除" OnClientClick="return confirm('確定刪除?');" onclick="lbtnDelete_Click" />
                                    
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"  />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        <PagerSettings FirstPageText="[First]" LastPageText="[Last]" 
                            NextPageText="Next" PageButtonCount="5"
                                    PreviousPageText="Previous"/>
            
        </asp:GridView>
    
    </div>
</asp:Content>

    
    

