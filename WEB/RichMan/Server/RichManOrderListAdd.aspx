﻿<%@ Page Language="C#" MasterPageFile="~/RichMan/Server/WebLayout.Master" AutoEventWireup="true" CodeBehind="RichManOrderListAdd.aspx.cs" Inherits="WEB.RichMan.Server.RichManOrderListAdd" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    $(document).ready(function() {
    $("a.titleMenu").eq(2).addClass("on");
    });
    </script>
    
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            
        </Scripts>
</asp:ScriptManager>

    <div>
        <asp:Label ID="lblAlert" style="color:Red;font-size:16pt" runat="server"></asp:Label>
    </div>
 <div>
        <div style=" width:400px">
        <strong style="color:Red">＊為必填欄位</strong><br/>
        <ol>
        <li>
            <span style="color:Red; font-size:18pt">＊</span><strong>申請土石流大富翁</strong><br/>
            <asp:DropDownList runat="server" ID="ddlAmount" ></asp:DropDownList>
            份
        </li>

        <li>
            <strong>土石流大富翁推廣對象？</strong><br/>
            <span>小學</span>
            <asp:DropDownList runat="server" ID="ddlLevel" ></asp:DropDownList>
            年級學生，或其他：<asp:TextBox ID="txtLevel" runat="server"></asp:TextBox>
        </li>

        <li>
            <strong>預計何時安排土石流大富翁輔助教學？</strong><br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:DropDownList runat="server" ID="ddl3Year" 
                    onselectedindexchanged="ddl3Year_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddl3Mouth" 
                    onselectedindexchanged="ddl3Mouth_SelectedIndexChanged" ></asp:DropDownList> / 
                <asp:DropDownList AutoPostBack="true" runat="server" ID="ddl3Day" ></asp:DropDownList>
            </ContentTemplate>
            </asp:UpdatePanel>
        
        </li>
        
        <li>
            <strong>將運用於何種學科或課程，作為教學輔助教材?</strong>
            
            <br />
            <asp:TextBox ID="txt4CourseContent" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
        </li>
        
        <li>
            <strong>請填寫聯繫資料，便於後續教育推廣交流，謝謝！</strong>            
            <table>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>服務單位：</td>
                    <td><asp:TextBox ID="txt5School" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>姓名：</td>
                    <td><asp:TextBox ID="txt5Name" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>職稱：</td>
                    <td><asp:TextBox ID="txt5Job" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>聯絡電話：</td>
                    <td><asp:TextBox ID="txt5Tel" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>E-MAIL：</td>
                    <td><asp:TextBox ID="txt5Email" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>寄送地址：</td>
                    <td><asp:TextBox ID="txt5Address" runat="server" ></asp:TextBox></td>
                </tr>
            </table>
        </li>
        
        <li>
            <strong>其他</strong><br/>
            <asp:TextBox TextMode="MultiLine" ID="txt6Ect" Rows="5" runat="server"></asp:TextBox>
        </li>
    
    </ol></div>
    
    <div class="btnBlock">
        <asp:Button ID="btnOK" runat="server"  Text="送出申請" onclick="btnOK_Click"/>
    </div>
    </div>
    
</asp:Content>

