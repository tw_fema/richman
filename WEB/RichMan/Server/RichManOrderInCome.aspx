﻿<%@ Page Language="C#" MasterPageFile="~/RichMan/Server/WebLayout.Master" AutoEventWireup="true" CodeBehind="RichManOrderInCome.aspx.cs" Inherits="WEB.RichMan.Server.RichManOrderInCome" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    $(document).ready(function() {
    $("a.titleMenu").eq(1).addClass("on");
    });
    </script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            
        </Scripts>
</asp:ScriptManager>

    <div>
        <div style=" width:400px">
        <strong style="color:Red">＊為必填欄位</strong><br/>
        <ol>
        <li>
            <strong>土石流防災大富翁進貨數量</strong><br/>
            <span style="color:Red; font-size:18pt">＊</span>
           <asp:TextBox runat="server" 
                 onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" ID="txtAmount" 
                Width="70px"></asp:TextBox>
                <!--onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"-->
            份
        </li>
        <li>
            <strong>請填寫個人資料，便於後續追蹤，謝謝！</strong>            
            <table>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>姓名：</td>
                    <td><asp:TextBox ID="txt5Name" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>服務單位：</td>
                    <td><asp:TextBox ID="txt5Job" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>聯絡電話：</td>
                    <td><asp:TextBox ID="txt5Tel" runat="server" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span style="color:Red; font-size:18pt">＊</span>E-MAIL：</td>
                    <td><asp:TextBox ID="txt5Email" runat="server" ></asp:TextBox></td>
                </tr>               
            </table>
        </li>
    </ol></div>
    
    <div class="btnBlock">
        <asp:Button ID="btnOK" runat="server"  Text="送出" onclick="btnOK_Click"/>
    </div>
    </div>
    <br />
    <hr style="border-top:1px dotted #ccc;HEIGHT:0;"/>
    <asp:UpdatePanel runat="server" ID="updatePanel1" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
     <div class="TxtStyle2">
        申請年度:<asp:DropDownList AutoPostBack="true" ID="ddlYear" runat="server" 
            onselectedindexchanged="ddlYear_SelectedIndexChanged1"></asp:DropDownList> &nbsp
     </div>
     <div class="TxtStyle">
            總進貨量：<asp:Label  runat="server" ID="lblstatic_totalAmount" Text=""></asp:Label>
            庫存量：<asp:Label runat="server" ID="lblstatic_realAmount" Text=""></asp:Label>
            <!--尚可申請數量：--><asp:Label Visible="false" runat="server" ID="lblstatic_haveAmount" Text=""></asp:Label>
            <!--待寄送數量：--><asp:Label Visible="false" runat="server" ID="lblstatic_waitSendAmount" Text=""></asp:Label>
            <!--實際寄送數量：--><asp:Label Visible="false" runat="server" ID="lblstatic_sendAmount" Text=""></asp:Label>
            <!--目前申請總量：--><asp:Label Visible="false" runat="server" ID="lblstatic_requireAmount" Text=""></asp:Label>
        </div>
        <br/>
     
                <asp:GridView ID="gvBearList" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="98%" 
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" 
            BorderWidth="1px" EmptyDataText="無資料"  DataKeyNames="ID"
                        AllowPaging="True" PageSize="10"
            onrowdatabound="gvBearList_RowDataBound" 
            onpageindexchanging="gvBearList_PageIndexChanging1" Font-Size="13px" >
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:TemplateField HeaderText="項次">
                                <itemtemplate>
                                    <%# Container.DataItemIndex + 1%>
                                    <asp:HiddenField ID="hiddenID" runat="server" Value='<%# Eval("ID") %>' />
                                </itemtemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            
                                                        
                            <asp:TemplateField HeaderText="姓名">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="電話">
                                <ItemTemplate>
                                    <asp:Label ID="lblTel" runat="server" Text='<%# Eval("Tel") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="服務單位"  HeaderStyle-HorizontalAlign ="Center" >
                                <ItemTemplate>
                                    <asp:Label ID="lblSchool" runat="server" Text='<%# Eval("School") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            

                             <asp:TemplateField HeaderText="進貨時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="進貨數量">
                                <ItemTemplate>
                                    <asp:Label ID="lblSendAmount" runat="server" Text='<%# Eval("RequireAmount") %>'></asp:Label>
                                    <asp:HiddenField ID="hiddenProcessStatus" runat="server" Value='<%# Eval("ProcessStatus") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"  />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        <PagerSettings FirstPageText="[First]" LastPageText="[Last]" 
                            NextPageText="Next" PageButtonCount="5"
                                    PreviousPageText="Previous"/>
            
        </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    



</asp:Content>




