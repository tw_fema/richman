﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace WEB.RichMan.Server
{
    public partial class RichManOrderList : System.Web.UI.Page
    {
        static int static_totalAmount;
        static int static_sendAmount;
        static int static_requireAmount;
        static int static_realAmount;
        static int static_haveAmount;
        static int static_waitSendAmount;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies["UID"];
            if (cookie == null)
            {
                Response.Redirect("~/RichMan/Server/login.aspx");
            }

            string uid = Request.Cookies["UID"].Value;
            if (uid == null || uid == "")
                Response.Redirect("~/RichMan/Server/login.aspx");


            if (!IsPostBack)
            {


                setddlYear();
                setddlViewStatus();
                setddlProcessStatus();

                //預設為最新一年資料
                int nowYear = DateTime.Now.Year;
                ddlYear.SelectedValue = nowYear.ToString();


                Query();

            }

            //當庫存量小於申請數量不開放申請
            DataTable dtTotal = Entity.RichMan.RichMan.GetALLBearList();
            //庫存
            int? totalAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已收入").Sum(obj => obj.Field<int?>("RequireAmount"));
            if (totalAmount != null)
            {
                totalAmount = Convert.ToInt32(totalAmount);
            }

            //全部申請數量
            //var objs = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") != "已收入");

            int? StatusZero_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "待處理").Sum(obj => obj.Field<int?>("RequireAmount"));
            int? StatusOne_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "處理中").Sum(obj => obj.Field<int?>("RequireAmount"));
            int? StatusTwo_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已送出").Sum(obj => obj.Field<int?>("SendAmount"));

            if (StatusZero_RequireAmount != null)
                StatusZero_RequireAmount = Convert.ToInt32(StatusZero_RequireAmount);

            if (StatusOne_RequireAmount != null)
                StatusOne_RequireAmount = Convert.ToInt32(StatusOne_RequireAmount);

            if (StatusTwo_RequireAmount != null)
                StatusTwo_RequireAmount = Convert.ToInt32(StatusTwo_RequireAmount);



            static_totalAmount = totalAmount != null ? Convert.ToInt32(totalAmount) : 0;
            static_requireAmount = (StatusZero_RequireAmount != null ? Convert.ToInt32(StatusZero_RequireAmount) : 0) + (StatusOne_RequireAmount != null ? Convert.ToInt32(StatusOne_RequireAmount) : 0) + (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);
            static_sendAmount = (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);


            lblstatic_totalAmount.Text = static_totalAmount.ToString();
            lblstatic_requireAmount.Text = static_requireAmount.ToString();



            lblstatic_sendAmount.Text = static_sendAmount.ToString();
            if (static_totalAmount - static_sendAmount == 0)
            {
                lblstatic_realAmount.Text = "0";
            }
            else
            {
                lblstatic_realAmount.Text = (static_totalAmount - static_sendAmount).ToString();
            }

            //尚可申請數量
            static_haveAmount = static_totalAmount - static_requireAmount > 0 ? static_totalAmount - static_requireAmount : 0;
            lblstatic_haveAmount.Text = static_haveAmount.ToString();

            //待寄送數量
            //static_waitSendAmount = static_requireAmount - static_sendAmount > 0 ? static_requireAmount - static_sendAmount : 0;
            lblstatic_waitSendAmount.Text = ((StatusZero_RequireAmount != null ? StatusZero_RequireAmount : 0) + (StatusOne_RequireAmount != null ? StatusOne_RequireAmount : 0)).ToString();



        }

        void setddlProcessStatus()
        {
            ddlProcessStatus.Items.Add(new ListItem("全部", "all"));
            ddlProcessStatus.Items.Add(new ListItem("待處理", "0"));
            ddlProcessStatus.Items.Add(new ListItem("處理中", "1"));
            ddlProcessStatus.Items.Add(new ListItem("已送出", "2"));
            ddlProcessStatus.Items.Add(new ListItem("重複申請", "3"));
            //ddlProcessStatus.Items.Add(new ListItem("已收入", "9"));
        }


        void setddlViewStatus()
        {
            ddlViewStatus.Items.Add(new ListItem("全部", "all"));
            ddlViewStatus.Items.Add(new ListItem("前端申請", "0"));
            ddlViewStatus.Items.Add(new ListItem("後端申請", "1"));
        }

        void setddlYear()
        {
            if (ddlYear.Items.Count > 0)
                ddlYear.Items.Clear();
            DataTable dt = Entity.RichMan.RichMan.GetServerBearYearList();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlYear.Items.Add(new ListItem((Convert.ToInt32(dr["createDate"]) - 1911).ToString(), dr["createDate"].ToString()));
                }
            }
            ddlYear.Items.Insert(0, new ListItem("全部", "all"));

        }

        void Query()
        {
            DataTable dt = Entity.RichMan.RichMan.GetServerBearList(ddlYear.SelectedValue, ddlViewStatus.SelectedValue, ddlProcessStatus.SelectedValue);
            gvBearList.DataSource = dt;
            gvBearList.DataBind();


            if (ddlProcessStatus.SelectedItem.Text != "全部")
            {
                //var objs = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") != "已收入");

                /*
                lblConditionRequireAmount.Text = string.Format("申請數量共:{0}份", dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") !="已收入").Sum(obj => obj.Field<int?>("RequireAmount")));
                lblConditionSendAmount.Text = string.Format("實際寄送數量共:{0}份", dt.AsEnumerable().Sum(o => o.Field<int?>("SendAmount")));
                lblConditionIncomeAmount.Text = string.Format("實際寄送數量共:{0}份", dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已收入").Sum(o => o.Field<int?>("SendAmount")));
                 */

            }


            //UpdatePanel1.Update();


        }

        protected void gvBearList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Web.UI.WebControls.Label lblProcessStatusTxt = e.Row.FindControl("lblProcessStatusTxt") as Label;
                System.Web.UI.WebControls.HiddenField hiddenSendDate = e.Row.FindControl("hiddenSendDate") as HiddenField;
                System.Web.UI.WebControls.HiddenField hiddenProcessStatus = e.Row.FindControl("hiddenProcessStatus") as HiddenField;

                //處理狀態為 已送出 : 2
                if (hiddenProcessStatus.Value == "2")
                {
                    //lblProcessStatusTxt.Text += "<br/>寄送時間:" + (Convert.ToDateTime(hiddenSendDate.Value).Year - 1911).ToString() + "/" + Convert.ToDateTime(hiddenSendDate.Value).ToString("MM/dd");
                }

                System.Web.UI.WebControls.Label lblCreateDate = e.Row.FindControl("lblCreateDate") as Label;
                string CreateDate = lblCreateDate.Text;
                lblCreateDate.Text = (Convert.ToDateTime(lblCreateDate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(lblCreateDate.Text).ToString("MM/dd HH:mm");

                System.Web.UI.WebControls.Label lblSendDate = e.Row.FindControl("lblSendDate") as Label;
                if (lblSendDate.Text != string.Empty)
                {
                    lblSendDate.Text = (Convert.ToDateTime(lblSendDate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(lblSendDate.Text).ToString("MM/dd");
                }


                System.Web.UI.WebControls.HiddenField hiddenID = e.Row.FindControl("hiddenID") as HiddenField;
                HyperLink hyModify = e.Row.FindControl("hyModify") as HyperLink;

                ImageButton lbtnDelete = e.Row.FindControl("lbtnDelete") as ImageButton;

                if (hiddenProcessStatus.Value == "9")
                {
                    hyModify.Visible = false;
                    lbtnDelete.Visible = false;

                }
                else if (hiddenProcessStatus.Value == "2")
                {
                    hyModify.Visible = false;
                    lbtnDelete.Visible = false;
                }

                if ((DateTime.Now.Year).ToString() != Convert.ToDateTime(CreateDate).Year.ToString())
                {

                    hyModify.Visible = false;
                    lbtnDelete.Visible = false;
                }
                if (hiddenProcessStatus.Value == "0")
                {
                    hyModify.Visible = true;
                    lbtnDelete.Visible = true;

                }
                else  //20140108 cheung 新增 力行: 不管狀態為何,還是要能夠修改
                {
                    hyModify.Visible = true;
                    lbtnDelete.Visible = true;
                }
                //////20140108 cheung 新增結束

                lbtnDelete.Attributes["key"] = hiddenID.Value;
                hyModify.NavigateUrl = string.Format("RichManOrderListEdit.aspx?id={0}", hiddenID.Value);






            }
        }



        protected void lbtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton lbtnDelete = sender as ImageButton;
            string id = lbtnDelete.Attributes["key"].ToString();

            Entity.RichMan.RichMan objBear = new Entity.RichMan.RichMan();
            objBear.ID = Convert.ToInt32(id);
            objBear.deleteUserID = Convert.ToInt32(Request.Cookies["UID"].Value);
            Entity.RichMan.RichMan.Delete(objBear);

            Query();
        }



        protected void ddlYear_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Query();
        }

        protected void ddlViewStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            Query();
        }

        protected void ddlProcessStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            Query();
        }



        protected void gvBearList_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            gvBearList.PageIndex = e.NewPageIndex;
            Query();
        }




    }
}
