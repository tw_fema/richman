﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;

namespace WEB.RichMan.Server
{
    public partial class RichManOrderInCome : System.Web.UI.Page
    {

        static int static_totalAmount;
        static int static_sendAmount;
        static int static_requireAmount;
        static int static_realAmount;
        static int static_haveAmount;
        static int static_waitSendAmount;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            HttpCookie cookie = Request.Cookies["UID"];
            if (cookie == null)
            {
                Response.Redirect("~/RichMan/Server/login.aspx");
            }
              
            
            string uid = Request.Cookies["UID"].Value;
            if (uid == null || uid == "")
                Response.Redirect("~/RichMan/Server/login.aspx");
            
            if (!IsPostBack)
            {
                setddlTotal();
                setddlYear();

                

                Query();

            }

            


            //當庫存量小於申請數量不開放申請
            DataTable dtTotal = Entity.RichMan.RichMan.GetALLBearList();
            //庫存
            int? totalAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已收入").Sum(obj => obj.Field<int?>("RequireAmount"));
            if (totalAmount != null)
            {
                totalAmount = Convert.ToInt32(totalAmount);
            }

            //全部申請數量
            //var objs = dt.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") != "已收入");

            int? StatusZero_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "待處理").Sum(obj => obj.Field<int?>("RequireAmount"));
            int? StatusOne_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "處理中").Sum(obj => obj.Field<int?>("RequireAmount"));
            int? StatusTwo_RequireAmount = dtTotal.AsEnumerable().Where(o => o.Field<string>("ProcessStatusTxt") == "已送出").Sum(obj => obj.Field<int?>("SendAmount"));

            if (StatusZero_RequireAmount != null)
                StatusZero_RequireAmount = Convert.ToInt32(StatusZero_RequireAmount);

            if (StatusOne_RequireAmount != null)
                StatusOne_RequireAmount = Convert.ToInt32(StatusOne_RequireAmount);

            if (StatusTwo_RequireAmount != null)
                StatusTwo_RequireAmount = Convert.ToInt32(StatusTwo_RequireAmount);



            static_totalAmount = totalAmount != null ? Convert.ToInt32(totalAmount) : 0;
            static_requireAmount = (StatusZero_RequireAmount != null ? Convert.ToInt32(StatusZero_RequireAmount) : 0) + (StatusOne_RequireAmount != null ? Convert.ToInt32(StatusOne_RequireAmount) : 0) + (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);
            static_sendAmount = (StatusTwo_RequireAmount != null ? Convert.ToInt32(StatusTwo_RequireAmount) : 0);


            lblstatic_totalAmount.Text = static_totalAmount.ToString();
            lblstatic_requireAmount.Text = static_requireAmount.ToString();



            lblstatic_sendAmount.Text = static_sendAmount.ToString();
            if (static_totalAmount - static_sendAmount == 0)
            {
                lblstatic_realAmount.Text = "0";
            }
            else
            {
                lblstatic_realAmount.Text = (static_totalAmount - static_sendAmount).ToString();
            }

            //尚可申請數量
            static_haveAmount = static_totalAmount - static_requireAmount > 0 ? static_totalAmount - static_requireAmount : 0;
            lblstatic_haveAmount.Text = static_haveAmount.ToString();

            //待寄送數量
            static_waitSendAmount = static_requireAmount - static_sendAmount > 0 ? static_requireAmount - static_sendAmount : 0;
            lblstatic_waitSendAmount.Text = static_waitSendAmount.ToString();

        }

        void setddlYear()
        {
            if (ddlYear.Items.Count > 0)
                ddlYear.Items.Clear();
            DataTable dt = Entity.RichMan.RichMan.GetServerBearYearList_InCome();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlYear.Items.Add(new ListItem((Convert.ToInt32(dr["createDate"]) - 1911).ToString(), dr["createDate"].ToString()));
                }
            }
            ddlYear.Items.Insert(0, new ListItem("全部", "all"));

        }

        protected void btnOK_Click(object sender, EventArgs e)
        {


            if (checkForm())
            {


                Entity.RichMan.RichMan obj = new Entity.RichMan.RichMan();
                setObj(obj);
                Entity.RichMan.RichMan.SaveServerInComeObj(obj);



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", "javascript:alert('己進貨');location.href='RichManOrderInCome.aspx'", true);
                //Response.Redirect("~/bear/Server/bearOrderList.aspx");
            }
        }

        void setddlTotal()
        {
            //開放匯入30000份
            /*
            ddlAmount.Items.Add(new ListItem("請選擇", ""));
            for (int i = 5; i <= 10000; i=i+5)
            {
                ddlAmount.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
             */ 

        }

        void setObj(Entity.RichMan.RichMan obj)
        {
            obj.Name = txt5Name.Text;
            obj.CreateDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            obj.Email = txt5Email.Text;
            obj.Job = txt5Job.Text;            

            //9:已匯入
            obj.ProcessStatus = 9;
            obj.RequireAmount = Convert.ToInt32(txtAmount.Text);
            obj.Tel = txt5Tel.Text;

            //1:後端伺服器
            obj.ViewStatus = 1;

        }

        

        
       
       

       

        bool checkForm()
        {
            //string tmp = "";
            if (txtAmount.Text == "")
            {
                
                //tmp = tmp + ",份數不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入份數!!');$('#ctl00_ContentPlaceHolder1_" + txtAmount.ID + "').focus();", true);
                return false;
            }
            else
            {
                var regex = new Regex("^[0-9]*[1-9][0-9]*$");
                if (!regex.IsMatch(txtAmount.Text))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('份數格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txtAmount.ID + "').focus();", true);
                    return false;
                }
            }
            
            if (txt5Name.Text == "")
            {
                //tmp = tmp + ",姓名不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入姓名!!');$('#ctl00_ContentPlaceHolder1_" + txt5Name.ID + "').focus();", true);
                return false;
            }
            if (txt5Job.Text == "")
            {
                //tmp = tmp + ",職稱不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入職稱!!');$('#ctl00_ContentPlaceHolder1_" + txt5Job.ID + "').focus();", true);
                return false;
            }
            if (txt5Tel.Text == "")
            {

                //tmp = tmp + ",電話不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入電話!!');$('#ctl00_ContentPlaceHolder1_" + txt5Tel.ID + "').focus();", true);
                return false;
            }
            else
            {
                var regex = new Regex("[0-9]{2,3}-[0-9]{6,8}#{0,1}[0-9]{0,4}$");
                if (!regex.IsMatch(txt5Tel.Text))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('電話格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txt5Tel.ID + "').focus();", true);
                    return false;
                }
            }
            if (txt5Email.Text == "")
            {
                //tmp = tmp + ",Email不得為空白";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('請輸入Email!!');$('#ctl00_ContentPlaceHolder1_" + txt5Email.ID + "').focus();", true);
                return false;
            }
            if (!CheckEmail(txt5Email.Text))
            {
                //tmp = tmp + "Email 格式錯誤";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "checkScript", "alert('Email 格式錯誤!!');$('#ctl00_ContentPlaceHolder1_" + txt5Email.ID + "').focus();", true);
                return false;
            }
            


            return true;
        }

        

        

        private bool CheckEmail(string EmailAddress)
        {

            string strPattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            if (System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, strPattern))
            { return true; }
            return false;
        }


        #region ********* 查詢區 start ********
        void Query()
        {
            DataTable dt = Entity.RichMan.RichMan.GetServerBearList_InCome(ddlYear.SelectedValue, "9");
            gvBearList.DataSource = dt;
            gvBearList.DataBind();


            updatePanel1.Update();

        }

        #endregion ********* 查詢區 end ********


        #region ***** 事件區 start *****
        protected void ddlYear_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Query();
        }

        protected void gvBearList_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            gvBearList.PageIndex = e.NewPageIndex;
            Query();
        }

        protected void gvBearList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                
                System.Web.UI.WebControls.HiddenField hiddenProcessStatus = e.Row.FindControl("hiddenProcessStatus") as HiddenField;
                System.Web.UI.WebControls.Label lblCreateDate = e.Row.FindControl("lblCreateDate") as Label;
                string CreateDate = lblCreateDate.Text;
                lblCreateDate.Text = (Convert.ToDateTime(lblCreateDate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(lblCreateDate.Text).ToString("MM/dd HH:mm");
            }
            #endregion ***** 事件區 end *****
        }

       

       
    }
}
