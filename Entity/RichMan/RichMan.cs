﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GISFCU.RD.Cool.DBLib;
using System.Data.SqlClient;
using System.Data;

namespace Entity.RichMan
{
    [Serializable]
    public class RichMan
    {

        //建構式
        public RichMan()
        {
 
        }

        //資料庫主key 流水號
        public int ID { get; set; }

        public string Name { get; set; }
        public string School { get; set; }
        public string Job { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ect { get; set; }
        public int RequireAmount { get; set; }
        public int SendAmount { get; set; }
        public string Rank { get; set; }
        public string CourseContent { get; set; }
        public DateTime PlanDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime SendDate { get; set; }
        public int ProcessStatus { get; set; }
        public int ViewStatus { get; set; }
        public string ItemNumber { get; set; }


        //刪除紀錄log用欄位
        public DateTime deleteDate { get; set; }
        public int deleteUserID { get; set; }

        #region ***** 選單區 start *****


        #endregion ***** 選單區 end *****

        #region ***** 查詢區 start *****

        static public DataTable GetServerBearList_InCome(string eastYear, string processStatus)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = @"select *,(case when ProcessStatus=0 then '待處理' when ProcessStatus=1 then '處理中' when ProcessStatus=2 then '已送出' when ProcessStatus=3 then '重複申請' when ProcessStatus=9 then '已收入' end) 
                                  as ProcessStatusTxt from tblRichMan where viewStatus=1  ";
            if (eastYear != "all")
            {
                sqlcmd.CommandText += " and datepart(yyyy,createDate)=@eastYear ";
                sqlcmd.Parameters.AddWithValue("@eastYear", eastYear);
            }
            
            if (processStatus.ToLower() != "all")
            {
                sqlcmd.CommandText += " and processStatus=@processStatus ";
                sqlcmd.Parameters.AddWithValue("@processStatus", processStatus);
            }

            sqlcmd.CommandText += "  order by createdate desc   ";

            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        /// <summary>
        /// 只查詢收入的份數
        /// </summary>
        /// <param name="eastYear"></param>
        /// <param name="viewStatus"></param>
        /// <param name="processStatus"></param>
        /// <returns></returns>
        static public DataTable GetServerBearList(string eastYear, string viewStatus, string processStatus)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select *,(case when ProcessStatus=0 then '待處理' when ProcessStatus=1 then '處理中' when ProcessStatus=2 then '已送出' when ProcessStatus=3 then '重複申請' when ProcessStatus=9 then '已收入' end) as ProcessStatusTxt from tblRichMan where 1=1 ";
            if (eastYear != "all")
            {
                sqlcmd.CommandText += " and datepart(yyyy,createDate)=@eastYear ";
                sqlcmd.Parameters.AddWithValue("@eastYear", eastYear);
            }
            if (viewStatus.ToLower() != "all")
            {
                sqlcmd.CommandText += " and viewStatus=@viewStatus ";
                sqlcmd.Parameters.AddWithValue("@viewStatus", viewStatus);
            }
            if (processStatus.ToLower() != "all")
            {
                sqlcmd.CommandText += " and processStatus=@processStatus ";
                sqlcmd.Parameters.AddWithValue("@processStatus", processStatus);
            }

            //2012.4.23 不塞選 已收入
            sqlcmd.CommandText += " and processStatus <> 9 ";

            sqlcmd.CommandText += "  order by createdate desc   ";

            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        static public DataTable GetALLBearList()
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select *,(case when ProcessStatus=0 then '待處理' when ProcessStatus=1 then '處理中' when ProcessStatus=2 then '已送出' when ProcessStatus=3 then '重複申請' when ProcessStatus=9 then '已收入' end) as ProcessStatusTxt from tblRichMan where 1=1 ";

            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        static public DataTable GetClientBearYearList()
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select distinct datepart(yyyy,createDate)createDate from tblRichMan where ProcessStatus <> 9   order by createDate desc ";
            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        static public DataTable GetServerBearYearList()
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select distinct datepart(yyyy,createDate)createDate from tblRichMan where 1=1  order by createDate desc";
            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        static public DataTable GetServerBearYearList_InCome()
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select distinct datepart(yyyy,createDate)createDate from tblRichMan where 1=1 and ProcessStatus=9  order by createDate desc";
            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        public static DataTable GetOneBearYearList(string id)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select *  from tblRichMan where id=@id ";
            sqlcmd.Parameters.AddWithValue("@id", id);
            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        static public DataTable GetClientBearList(string eastYear)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "select *,(case when ProcessStatus=0 then '待處理' when ProcessStatus=1 then '處理中' when ProcessStatus=2 then '已送出' when ProcessStatus=3 then '重複申請' end) as ProcessStatusTxt from tblRichMan where ProcessStatus <> 9  and viewStatus = 0 ";
            if (eastYear != "all")
            {
                sqlcmd.CommandText += " and datepart(yyyy,createDate)=@eastYear ";
                sqlcmd.Parameters.AddWithValue("@eastYear", eastYear);
            }

            sqlcmd.CommandText += "  order by createdate desc ";

            return SQLDBController.Select(sqlcmd, "DB246ConnectionString");
        }

        #endregion ***** 查詢區 end *****

        #region ***** 修改區 start *****

        static public void UpdateServerObj(RichMan obj, string processStatus)
        {
            SqlCommand sqlcmd = new SqlCommand();
            if (processStatus == "已送出")
            {
                sqlcmd.CommandText = string.Format(@"update tblRichMan set name='{0}',school='{1}',job='{2}',tel='{3}',email='{4}',Address='{5}',ect='{6}',RequireAmount='{7}',Rank='{8}',CourseContent='{9}',PlanDate='{10}',ProcessStatus={11},ViewStatus='{12}',sendAmount='{13}',sendDate='{14}',ItemNumber='{16}'  where id={15}"
                                    , obj.Name
                                    , obj.School
                                    , obj.Job
                                    , obj.Tel
                                    , obj.Email
                                    , obj.Address
                                    , obj.ect
                                    , obj.RequireAmount
                                    , obj.Rank
                                    , obj.CourseContent
                                    , obj.PlanDate.ToString("yyyy/MM/dd")
                                    , obj.ProcessStatus
                                    , obj.ViewStatus
                                    , obj.SendAmount
                                    , obj.SendDate.ToString("yyyy/MM/dd HH:mm:ss")
                                    , obj.ID
                                    , obj.ItemNumber);
            }
            else if (processStatus == "重複申請")
            {
                sqlcmd.CommandText = string.Format(@"update tblRichMan set name='{0}',school='{1}',job='{2}',tel='{3}',email='{4}',Address='{5}',ect='{6}',RequireAmount='{7}',Rank='{8}',CourseContent='{9}',PlanDate='{10}',ProcessStatus={11},ViewStatus='{12}',sendAmount={13},sendDate={14},ItemNumber='{16}'  where id={15}"
                                    , obj.Name
                                    , obj.School
                                    , obj.Job
                                    , obj.Tel
                                    , obj.Email
                                    , obj.Address
                                    , obj.ect
                                    , obj.RequireAmount
                                    , obj.Rank
                                    , obj.CourseContent
                                    , obj.PlanDate.ToString("yyyy/MM/dd")
                                    , obj.ProcessStatus
                                    , obj.ViewStatus
                                    , System.Data.SqlTypes.SqlInt32.Null
                                    , System.Data.SqlTypes.SqlDateTime.Null
                                    , obj.ID
                                    , obj.ItemNumber);
            }
            else
            {
                sqlcmd.CommandText = string.Format(@"update tblRichMan set name='{0}',school='{1}',job='{2}',tel='{3}',email='{4}',Address='{5}',ect='{6}',RequireAmount={7},Rank='{8}',CourseContent='{9}',PlanDate='{10}',ProcessStatus={11},ViewStatus={12} where id={13}"
                                    , obj.Name
                                    , obj.School
                                    , obj.Job
                                    , obj.Tel
                                    , obj.Email
                                    , obj.Address
                                    , obj.ect
                                    , obj.RequireAmount
                                    , obj.Rank
                                    , obj.CourseContent
                                    , obj.PlanDate.ToString("yyyy/MM/dd")
                                    , obj.ProcessStatus
                                    , obj.ViewStatus
                                    , obj.ID);
            }

            SQLDBController.Update(sqlcmd, "DB246ConnectionString");


        }

        #endregion ***** 修改區 end *****

        #region ***** 新增區 start *****

        static public void SaveClientObj(RichMan obj)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = string.Format(@"insert into tblRichMan(name,school,job,tel,email,Address,ect,RequireAmount,Rank,CourseContent,PlanDate,CreateDate,ProcessStatus,ViewStatus)
                                  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},'{8}','{9}','{10}','{11}',{12},{13})"
                                , obj.Name
                                , obj.School
                                , obj.Job
                                , obj.Tel
                                , obj.Email
                                , obj.Address
                                , obj.ect
                                , obj.RequireAmount
                                , obj.Rank
                                , obj.CourseContent
                                , obj.PlanDate.ToString("yyyy/MM/dd")
                                , obj.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                                , obj.ProcessStatus
                                , obj.ViewStatus);

            SQLDBController.Insert(sqlcmd, "DB246ConnectionString");


        }

        static public void SaveServerObj(RichMan obj)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = string.Format(@"insert into tblRichMan(name,school,job,tel,email,Address,ect,RequireAmount,Rank,CourseContent,PlanDate,CreateDate,ProcessStatus,ViewStatus)
                                  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},'{8}','{9}','{10}','{11}',{12},{13})"
                                , obj.Name
                                , obj.School
                                , obj.Job
                                , obj.Tel
                                , obj.Email
                                , obj.Address
                                , obj.ect
                                , obj.RequireAmount
                                , obj.Rank
                                , obj.CourseContent
                                , obj.PlanDate.ToString("yyyy/MM/dd")
                                , obj.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                                , obj.ProcessStatus
                                , obj.ViewStatus);

            SQLDBController.Insert(sqlcmd, "DB246ConnectionString");


        }

        /// <summary>
        /// 管理者於後端新增小熊種樹份數
        /// </summary>
        /// <param name="obj"></param>
        static public void SaveServerInComeObj(RichMan obj)
        {
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = string.Format(@"insert into tblRichMan(name,job,tel,email,RequireAmount,CreateDate,ProcessStatus,ViewStatus)
                                  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')"
                                , obj.Name
                                , obj.Job
                                , obj.Tel
                                , obj.Email
                                , obj.RequireAmount
                                , obj.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                                , obj.ProcessStatus
                                , obj.ViewStatus);

            SQLDBController.Insert(sqlcmd, "DB246ConnectionString");


        }

        #endregion ***** 新增區 end *****

        #region ***** 刪除區 start *****

        public static void Delete(RichMan obj)
        {



            SqlCommand sqlcmd = new SqlCommand();

            sqlcmd.CommandText = "select * from tblRichMan where id=@id";
            sqlcmd.Parameters.AddWithValue("@id", obj.ID);
            DataTable dt = SQLDBController.Select(sqlcmd, "DB246ConnectionString");
            obj.Name = dt.Rows[0]["Name"].ToString();
            obj.School = dt.Rows[0]["School"].ToString();
            obj.Job = dt.Rows[0]["Job"].ToString();
            obj.Tel = dt.Rows[0]["Tel"].ToString();
            obj.Email = dt.Rows[0]["Email"].ToString();
            obj.Address = dt.Rows[0]["Address"].ToString();
            obj.ect = dt.Rows[0]["ect"].ToString();
            obj.RequireAmount = Convert.ToInt32(dt.Rows[0]["RequireAmount"]);
            obj.SendAmount = dt.Rows[0]["SendAmount"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["SendAmount"]) : 0;
            obj.Rank = dt.Rows[0]["Rank"].ToString();
            obj.CourseContent = dt.Rows[0]["CourseContent"].ToString();
            obj.PlanDate = Convert.ToDateTime(dt.Rows[0]["PlanDate"].ToString());
            obj.CreateDate = Convert.ToDateTime(dt.Rows[0]["CreateDate"].ToString());
            obj.SendDate = dt.Rows[0]["SendDate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["SendDate"]) : DateTime.MinValue;
            DateTime dd = DateTime.MinValue;
            obj.ProcessStatus = Convert.ToInt32(dt.Rows[0]["ProcessStatus"]);
            obj.ViewStatus = Convert.ToInt32(dt.Rows[0]["ViewStatus"].ToString());
            obj.deleteDate = DateTime.Now;
            //obj.deleteUserID = 0;

            //先存log
            sqlcmd.CommandText = string.Format(@"insert into tblRichMan_deleteLog(id,name,school,job,tel,email,Address,[ect],RequireAmount,Rank,CourseContent,PlanDate,CreateDate,ProcessStatus,ViewStatus,deleteDate,deleteUserID)
                                  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')"
                                , obj.ID
                                , obj.Name
                                , obj.School
                                , obj.Job
                                , obj.Tel
                                , obj.Email
                                , obj.Address
                                , obj.ect
                                , obj.RequireAmount
                                , obj.Rank
                                , obj.CourseContent
                                , obj.PlanDate.ToString("yyyy/MM/dd")
                                , obj.CreateDate.ToString("yyyy/MM/dd HH:mm:ss")
                                , obj.ProcessStatus
                                , obj.ViewStatus
                                , obj.deleteDate.ToString("yyyy/MM/dd HH:mm:ss")
                                , obj.deleteUserID);

            SQLDBController.Insert(sqlcmd, "DB246ConnectionString");

            if (obj.SendDate != DateTime.MinValue)
            {
                sqlcmd.CommandText = string.Format("update tblRichMan_deleteLog set sendAmount='{0}',sendDate='{1}' where id={2}", obj.SendAmount, obj.SendDate.ToString("yyyy/MM/dd HH:mm:ss"), obj.ID);
                SQLDBController.Update(sqlcmd, "DB246ConnectionString");
            }


            //刪除資料列
            sqlcmd.CommandText = string.Format(@"delete from tblRichMan where id={0} "
                                , obj.ID
                                );

            SQLDBController.Delete(sqlcmd, "DB246ConnectionString");
        }

        #endregion ***** 刪除區 end *****

        


       

        

        




        

        

    }
}
