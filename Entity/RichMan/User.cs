﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using GISFCU.RD.Cool.DBLib;

namespace Entity.RichMan
{
	/// <summary>
	/// 使用者資訊類別
	/// </summary>
	public class User
	{
		public string Uid {
			get;
			set;
		}

		public string Account {
			get;
			set;
		}

		public string Password {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string TelOffice {
			get;
			set;
		}

		public string TelHome {
			get;
			set;
		}

		public string Email {
			get;
			set;
		}

		public string Org {
			get;
			set;
		}

		public string Cellphone {
			get;
			set;
		}

		public User() {
		}

		public static User Login(string Account, string Password) {
			SqlCommand sqlcmd = new SqlCommand();
			sqlcmd.CommandText = "select * from tblRichManAccount where account=@Account and Password=@Password ";
			sqlcmd.Parameters.AddWithValue("@Account", Account);
			sqlcmd.Parameters.AddWithValue("@Password", Password);

			DataTable dt = SQLDBController.Select(sqlcmd, "DB246ConnectionString");

			if (dt != null && dt.Rows.Count > 0) {
				User pUser = new User();
				pUser.Uid = dt.Rows[0]["UID"].ToString();
				pUser.Name = Convert.ToString(dt.Rows[0]["Name"]);
				pUser.Account = Convert.ToString(dt.Rows[0]["Account"]);
				return pUser;
			}
			else {
				return null;
			}

		}

		static public void SaveServerObj(User obj) {
			SqlCommand sqlcmd = new SqlCommand();
			sqlcmd.CommandText = string.Format(@"insert into tblRichManAccount(Account,Password,Name,TelOffice,TelHome,Email,Org,Cellphone)
                                  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')"
								, obj.Account
								, obj.Password
								, obj.Name
								, obj.TelOffice
								, obj.TelHome
								, obj.Email
								, obj.Org
								, obj.Cellphone
								);

			SQLDBController.Insert(sqlcmd, "DB246ConnectionString");
		}

	}
}